terraform {
  required_version = ">= 0.14.0"
}

data "http" "hello" {
  url = "https://riskiwah.xyz/"

  request_headers = {
    Accept = "application/json"
  }
}

output "hello" {
  value = data.http.hello.body
}

module "dev-echoing" {
  source = "git::https://gitlab.com/terraform96/core.git//modules/echoing"
  text = {
    env      = "dev"
    message  = "hello world! - v2"
    versions = ">= 0.14.0"
    another = "bmessage!23"
  }
}
